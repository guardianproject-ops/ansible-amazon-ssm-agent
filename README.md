# ansible-amazon-ssm-agent

Installs the amazon-ssm-agent on Debian systems from [the snap package](https://snapcraft.io/amazon-ssm-agent)

## Requirements

* Debian
* Ansible >= 2.8

## Role Variables

```
none
```


## Example Playbook

```yaml
- hosts: servers
  roles:
    - ansible-amazon-ssm-agent
```

# Testing

```
molecule test --all
```

# License

[GNU Affero General Public License (AGPL) v3+](https://www.gnu.org/licenses/agpl-3.0.en.html)

# Author Information

* Abel Luck <abel@guardianproject.info> of [Guardian Project](https://guardianproject.info)
