import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_directories(host):
    dirs = [
            "/snap/amazon-ssm-agent",
    ]
    for dir in dirs:
        d = host.file(dir)
        assert d.is_directory
        assert d.exists


def test_snap_installed(host):
    host.check_output("snap services | grep amazon-ssm-agent")


def test_service(host):
    s = host.service("snapd")
    assert s.is_running
